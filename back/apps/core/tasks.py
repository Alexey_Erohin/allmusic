import glob, os
from importlib import import_module

from celery import shared_task

from twisted.internet import reactor

from scrapy import signals, log
from scrapy.crawler import Crawler
from scrapy.settings import Settings


def get_settings():
    return Settings(
        {'ITEM_PIPELINES': {
            # 'scrapy.contrib.pipeline.images.ImagesPipeline'     : 2,

            'apps.scraping.pipelines.save_to_db.Pipeline'         : 1,

            },
         'IMAGES_STORE': '/home/nervosa/Downloads/albums_scraped/images',
         # 'CLOSESPIDER_ITEMCOUNT': 3,
         'DOWNLOAD_DELAY': 0.2,
         'IMAGES_THUMBS': {
             'small'    : ( 78, 128),
             'medium'   : (235, 383)
         }
         })


def get_spiders():
    # load all spiders from spiders folder
    os.path.join(os.path.dirname(os.path.dirname(__file__)), 'scraping/spiders', "*.py")
    files = glob.glob(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'scraping/spiders', "*.py"))
    files = [ os.path.splitext(os.path.basename(file))[0] for file in files]
    files = [file for file in files if file not in ['__init__', 'base']]

    spiders = {}
    for file in files:
        mod = import_module('apps.scraping.spiders.'+file)
        # get class and create object
        spiders[getattr(mod, 'Spider').name] = getattr(mod, 'Spider')

    return spiders


def run_crawler(spider_name):

    spiders = get_spiders()
    settings = get_settings()

    crawler = Crawler(settings)
    crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
    crawler.configure()

    crawler.crawl(spiders[spider_name]())

    crawler.start()
    # log.start('/home/nervosa/LOG_SCRAPY.log', log.DEBUG)
    reactor.run()
