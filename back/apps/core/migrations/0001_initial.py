# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Style',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128)),
                ('file', models.FileField(upload_to=b'')),
                ('quality', models.CharField(max_length=64)),
                ('duration', models.TimeField()),
                ('album', models.ForeignKey(to='core.Album', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('type', models.CharField(max_length=3, choices=[(b'Bnd', b'Band'), (b'Prs', b'Person'), (b'Lbl', b'Lable')])),
                ('group', models.ForeignKey(to='core.Unit', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='track',
            name='authors',
            field=models.ManyToManyField(related_name='authors', to='core.Unit'),
        ),
        migrations.AddField(
            model_name='track',
            name='cover',
            field=models.ForeignKey(to='core.Track', null=True),
        ),
        migrations.AddField(
            model_name='track',
            name='performers',
            field=models.ManyToManyField(related_name='performers', to='core.Unit'),
        ),
        migrations.AddField(
            model_name='track',
            name='styles',
            field=models.ManyToManyField(to='core.Style'),
        ),
        migrations.AddField(
            model_name='album',
            name='lable',
            field=models.ForeignKey(to='core.Unit'),
        ),
    ]
