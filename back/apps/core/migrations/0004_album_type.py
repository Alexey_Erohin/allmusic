# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_albumimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='type',
            field=models.CharField(max_length=30, null=True, choices=[(b'full', b'Album'), (b'ep', b'EP'), (b'single', b'Single'), (b'maxi_single', b'Maxi-single'), (b'compilation', b'Compilation'), (b'bestof', b'Best of')]),
        ),
    ]
