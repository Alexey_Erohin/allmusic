# -*- coding: utf-8 -*-
from django.conf import settings
from django.db.models.fields.files import ImageFieldFile, FileField
from django.db.models import Model
from django.db.models import ForeignKey
from django.db.models import CharField, URLField, ImageField, DateTimeField, IntegerField, \
    BooleanField, ManyToManyField, FloatField, PositiveSmallIntegerField, TimeField
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


UNIT_TYPES = (
        ('Bnd', 'Band'),
        ('Prs', 'Person'),
        ('Lbl', 'Lable')
    )

ALBUM_TYPES = (
    ('full', 'Album'),
    ('ep', 'EP'),
    ('single', 'Single'),
    ('maxi_single', 'Maxi-single'),
    ('compilation', 'Compilation'),
    ('bestof', 'Best of')
)

class Unit(Model):

    name = CharField(max_length=128)
    type = CharField(max_length=3, choices=UNIT_TYPES)
    group = ForeignKey('self', null=True)


class Album(Model):
    lable = ForeignKey(Unit)
    type = CharField(max_length=30, choices=ALBUM_TYPES, null=True)


class AlbumImage(Model):
    image = ImageField()
    album = ForeignKey(Album)


class Style(Model):
    name = CharField(max_length=64)


class Track(Model):
    title      = CharField(max_length=128)
    album      = ForeignKey(Album, null=True)
    authors    = ManyToManyField(Unit, related_name='authors')
    performers = ManyToManyField(Unit, related_name='performers')
    styles     = ManyToManyField(Style)

    cover      = ForeignKey('self', null=True)

    file = FileField()

    quality = CharField(max_length=64)

    duration = TimeField()


class CrawledWebsite(Model):
    name    = CharField(max_length=128, unique=True)
    domain  = URLField(unique=True)


class Spider(Model):
    name    = CharField(max_length=128)









