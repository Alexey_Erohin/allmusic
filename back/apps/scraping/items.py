from scrapy.item import Field, Item


class AlbumItem(Item):
    page = Field()
    unit_name = Field()