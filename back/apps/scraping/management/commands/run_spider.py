from django.core.management.base import BaseCommand
from django.conf import settings
from apps.core.tasks import run_crawler


class Command(BaseCommand):

    args = 'Some args'
    help = 'Run parsing.'

    def handle(self, *args, **options):
        run_crawler('dark-world.ru')
