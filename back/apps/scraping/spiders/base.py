from scrapy.contrib.spiders import CrawlSpider
from ..items import AlbumItem
from apps.core.models import Album, Track, Unit, Style, CrawledWebsite, Spider


class BaseSpider(CrawlSpider):

    def __init__(self):
        crawled_website, created = CrawledWebsite.objects.get_or_create(domain=self.name, defaults={'name': self.name})
        spider, created = Spider.objects.get_or_create(name=self.name)

        # TODO: probably log setup will be needed

        super(BaseSpider, self).__init__()


    def getItem(self, response):
        item = AlbumItem()
        item['page'] = response.url
        return item