from scrapy import Request
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors import LinkExtractor

from base import BaseSpider


class Spider(BaseSpider):

    name = "dark-world.ru"
    allowed_domains = ["dark-world.ru"]
    start_urls = ["http://dark-world.ru/albums", ]
    rules = (
        Rule(LinkExtractor(allow=[r'\/?&page=\d+']), follow=True),
        Rule(LinkExtractor(allow=[r'\/?page=\d+']), follow=True),
        Rule(LinkExtractor(allow=[r'\/albums\/[a-zA-Z0-9_-]+']), 'parse_album'),
    )

    def parse_album(self, response):
        print("Parsing an album....")
        item = self.getItem(response)
        item['unit_name'] = response.xpath('//table[@id="album-details"]/tr/td/a[@id="showOtherMaterials"]/text()').extract()
        return item
