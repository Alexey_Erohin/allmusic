# -*- coding: utf-8 -*-
from django.db import IntegrityError
from scrapy.exceptions import DropItem
from apps.core.models import Album, Unit


class Pipeline(object):

    def open_spider(self, spider):
        pass

    def process_item(self, item, spider):
        return item

    def close_spider(self, spider):
        pass